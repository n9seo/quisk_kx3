# quisk_kx3

#### Sets up Quisk for use with KX3 control via hamlib

##### Requirements

* I/Q Soundcard suitable for KX3
* KX3
* rigctld setup KX3 

##### Setup

Quisk setup in Config->Radios add a new Softrock USB and name it something like KX3.

You will use the `quisk_hardware_kx3.py` in your radio ("KX3") and set this as the Hardware file path.


##### Features

This hardware file should set modes like standard AM|FM|CW|USB|LSB etc as well as DGT-U and DTG-L as PKTUSB AND PKTLSB.
Am looking to add other modes to this but need to find out from quisk devs if thats even an option to customize within these
files here in this repo.

Filter sizes at the moment can only be changed from quisk bu setting them
to a supported rig size and toggling one of the supported modes. Looking into alternative means to do this.


